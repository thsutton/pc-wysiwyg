PC WYSIWYG
==========

The [PC WYSIWYG][1] [Drupal][2] module installs and configures WYSIWYG editing
functionality as required for sites based on the [PC CMS][3] installation
profile.

[1]: https://bitbucket.org/thsutton/pc-wysiwyg
[2]: https://drupal.org
[3]: https://bitbucket.org/thsutton/pc-cms
